# Thinking Distributed

## Why Distributed Networking

We've seen what happens when large corporations hold our data. The alternative is to spread the load among those who use the network.
