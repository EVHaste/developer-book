# Private Routing

A private route allows apps on a Veilid network to create an endpoint that the consuming app can send to, but provides no other information.  This differs from normal Veilid operation by hiding the address of the route creator during real time communication.

Private routes differ from safety routes.  A safety route is a contextual flag on a routing context, designed to protect the sender. The developer using the context must know that communication using the context will protect the identity of the local node. As such, the flag just tells the context that when it needs to route messages, it should construct and use safety routes for them rather than directly routing them. 

Private routes are created on request by the application,and are designed to protect the receiver. A private route is requested by the application when it wants to receive messages but still have its identity protected. Because the app needs to maintain it, it's much less of a transparent process.

The process for private route use is more or less:

1. Get a private blob for an endpoint from the DHT.
2. Import the private route blob and get a route ID.
 * If there is an error, the route is not present, so wait for change and go back to the beginning.
3. Use the route ID provided to communicate with the app.
 * If error, go back to the beginning.
4. Complete the communication.
5. Free the route id.

## Creating a Private Route

A Veilid app can create a private route with new_private_route().  This will, using default encryption, save a blob to the DHT that can then be collected by the consuming app and used to address messages back to the creator.

In Rust, the new_private_route stands up a routeid and blob for storage.

```
pub async fn newPrivateRoute() -> APIResult<VeilidRouteBlob> {
    let veilid_api = get_veilid_api()?;

    let (route_id, blob) = veilid_api.new_private_route().await?;

    let route_blob = VeilidRouteBlob { route_id, blob };
    APIResult::Ok(route_blob)
}
```

In Python, creating a new private route uses the JSON API, which is recommended for applications that use JSON rather than native code.  It looks something like this:

```
async with api:
    routeid, blob = await api.new_private_route()
```

You can see in that simple example that the function returns a route id, and a blob to store a message.  All of this is stored in the DHT which is hidden complexity. 

## Consuming a private route

For the consuming app, the message can be dropped in the blob, again without the complexity added by communicating directly with the DHT. Use import_remote_private_route() for this capability. import_remote_private_route() returns a route_id, which is used as the address for the recipient in all calls that send data to them. The returned route_id is ephemeral (cannot be used after restarting the node or transmitted to another node to be used), is not the same between multiple calls with the same private route blob, and is generally just a handle to the specified route in the local node's memory.

```
async with api:
    async with routingcontext:
            privateroute = await api.import_remote_private_route(blob)
            message = b"Hello world!"
            await routingcontext.app_message(privateroute, message)
```

## More information

There is a great example of using private routing in the Python API unit tests, found here:

https://gitlab.com/veilid/veilid/-/blob/main/veilid-python/tests/test_routing_context.py
