# Installation

1. The python chat application requires a headless Veilid node.  Find instructions [here](https://gitlab.com/veilid/veilid/-/blob/main/INSTALL.md).
2. Clone the [python-demo](https://gitlab.com/veilid/python-demo.git) project.
3. Install [poetry](https://python-poetry.org) if you haven't already.
4. Run `poetry install`
