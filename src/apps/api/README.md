# How to Use the Veilid API

Generally, the way to use the Veilid API is:

1. Determine how the Veilid node you're running is to be configured.
2. Determine how you want to respond to Veilid network updates, by writing a network update callback function.
3. Instantiate the Veilid API, applying the configuration and update callback.
4. Attach to the Veilid network.
5. Run your application until you're ready to stop, possibly getting and exposing current status from the API.
6. Detach from the Veilid network.
7. Shut down the Veilid API.

Every event which updates the network state that a node can be interested in is exposed to the application via a message sent to the app's update callback function. See <callback-messages.md> for more information, and the list of messages which Veilid may send.
